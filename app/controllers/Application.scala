package controllers


import play.api.libs.json.Json._
import play.api.libs.ws.{WSClient, WSRequest}
import play.api.mvc._
import javax.inject._

import model.Person
import play.api.Logger
import play.api.libs.json.Json
import play.twirl.api.Html

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{Await, duration}
import scala.concurrent.duration.Duration


@Singleton
class Application @Inject()(ws: WSClient) extends Controller {

  val url: String = "http://localhost:9000/addPerson"

  def index = Action { implicit request =>
    Ok(currentApi)
  }

  private def currentApi(implicit request: RequestHeader) = {
    toJson(Map(
      "root" -> request.uri
    ))
  }

  def callExternal = Action(parse.urlFormEncoded) { implicit data =>

    val outgoingRequest: WSRequest = ws.url(url)
    val future = outgoingRequest.post(Json.toJson(Person.fromMap(data.body)))
    val result = Await.result(future, Duration(5, duration.SECONDS))
    Ok(play.twirl.api.Html(result.body))
  }

  def form = Action {
    Ok(views.html.addperson())
  }
}
