package model

import play.api.libs.json.{Json, Writes}

case class Person(name: String, age: Int)

object Person {
  def apply(name: String, age: String): Person = new Person(name, age.toInt) //TODO is it a valid integer ?
  def fromMap(map: Map[String, Seq[String]]): Person = { //TODO validation would be useful
    Person(map("name").head,map("age").head)
  }

  implicit val writes: Writes[Person] = Json.writes[Person]
}

